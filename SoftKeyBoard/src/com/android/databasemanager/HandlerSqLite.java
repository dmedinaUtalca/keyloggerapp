package com.android.databasemanager;

//para trabajar con los ID en la base de datos;
import static android.provider.BaseColumns._ID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author David Medina Ortiz
 * @descripcion
 * Clase que contiene los comportamientos necesarios para establecer conexion con una base de datos interna,
 * permite hacer consultas, crear la conexion, insertar datos y eliminar informacion.
 * @version alpha
 */
public class HandlerSqLite extends SQLiteOpenHelper{

	/**
	 * @author David Medina Ortiz
	 * @description
	 * Constructor de la clase
	 * @param context
	 */
	public HandlerSqLite(Context context) {
		super(context, "testWords", null, 1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author David Medina Ortiz
	 * @description
	 * Iniciador de la base de datos...
	 * @param db
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String query = "CREATE TABLE palabras ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+" contenido TEXT, date TEXT);";
		db.execSQL(query);
	}

	/**
	 * @author David Medina Ortiz
	 * @description
	 * Actualizador de la base de datos...
	 * @param db	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS palabras");
		onCreate(db);
		
	}

	/**
	 * @author David Medina Ortiz
	 * @description
	 * Abre la conexion a la base de datos...
	 */
	public void abrir(){
		
		this.getWritableDatabase();
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Cierra la conexion a la base de datos...
	 */
	public void cerrar(){
	
		this.close();
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Inserta datos a la base de datos
	 * @param contenido
	 * @param fecha
	 */
	public void InterDB(String contenido, String fecha){
		
		ContentValues valores = new ContentValues();//crea un contentvalue
		
		//le ponemos los datos recibidos por parametros...
		valores.put("contenido", contenido);
		valores.put("date", fecha);
		this.getWritableDatabase().insert("palabras", null, valores);//hacemos la insercion a la base de datos...
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Elimina los elementosde la base de datos...
	 */	
	public void Eliminar(){
		
		onUpgrade(this.getWritableDatabase(), 1, 2);
	}
	/**
	 * @author David Medina
	 * @description
	 * Hace una consulta general a la base de datos, retornando los datos en un Arreglo...
	 * @return
	 */
	public String [] leer(){
		
		ArrayList<String> resultados = new ArrayList<String>();
		
		String columns[] = {_ID, "contenido", "date"};//obtenemos las columnas...
		
		//hacemos la consulta....
		Cursor c = this.getReadableDatabase().query("palabras", columns, null, null, null, null, null);
		
		c.moveToFirst();
		int id1, id2, id3;
		
		//obtenemos los ids de los campos del cursor...
		id1 = c.getColumnIndex(_ID);
		id2 = c.getColumnIndex("contenido");
		id3 = c.getColumnIndex("date");
		
		//ciclo de movimiento de cursor...
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
			
			String res = c.getString(id1)+" "+c.getString(id2)+" "+c.getString(id3);
			
			resultados.add(res);//agregamos resultado a lista parcial...
		}
		
		//generamos array con los datos obtenidos en la coleccion...
		String[] res_query = new String [resultados.size()];
		
		for (int i=0; i<resultados.size(); i++){
			
			res_query[i] = resultados.get(i);
		}
		
		return res_query;//se retorna el arreglo...
	}
	
	/**
	 * @author David Medina
	 * @description
	 * Genera el ranking global con los datos existentes en la base de datos
	 * @return
	 */
	public String [] RankingGlobal(){
		
		String [] total = this.leer();//obtenemos la totalidad de los datos...
		ArrayList<String> words = new ArrayList<String>();//tenemos arreglo parcial...
		ArrayList<String> words_sin_repetir = new ArrayList<String>();//para contener las palabras no repetidas...
		
		//ciclo para obtener solo la palabra...
		for (int i=0; i<total.length; i++){
			
			 String[] split_sep = total[i].split(" ");//separamos por espacio...	 
			 words.add(split_sep[1]);//agregamos el elemento en posicion 1 al arreglo...
			 words_sin_repetir.add(split_sep[1]);
		}
		
		//esto es para eliminar los elementos repetidos...
		// add elements to al, including duplicates 
		HashSet<String> hs = new HashSet<String>();
		hs.addAll(words_sin_repetir); 
		words_sin_repetir.clear(); 
		words_sin_repetir.addAll(hs);

		//ahora genero un String [] de las palabras no repetidas...
		String[] solas = new String[words_sin_repetir.size()];
		for (int i=0; i<words_sin_repetir.size(); i++){
			
			solas[i] = words_sin_repetir.get(i);
		}
		
		//ahora cuento cada repeticion...
		for (int i=0; i<solas.length; i++){
		
			int cont=0;
			for (int j=0; j<words.size(); j++){
				
				if (words.get(j).equals(solas[i])){
					cont++;
				}
			}
			solas[i] = ""+cont+" "+solas[i];
		}
		
		Arrays.sort(solas);
		
		//creo arreglo para ordenarlos de mayor a menor...
		String [] solas2 = new String[solas.length];
		
		int cont = solas.length-1;
		
		for (int i=0; i<solas.length; i++){
			
			solas2[i] = solas[cont];
			cont--;
		}
		
		return solas2;

	}
	
	/**
	 * @author David Medina
	 * @description
	 * Genera el ranking por fecha con los datos existentes en la base de datos
	 * @param date
	 * @return
	 */
	public String [] RankingFecha(String date){
		
		String [] total = this.leer();//obtenemos la totalidad de los datos...
		ArrayList<String> words = new ArrayList<String>();//tenemos arreglo parcial...
		ArrayList<String> words_sin_repetir = new ArrayList<String>();//para contener las palabras no repetidas...
		
		//ciclo para obtener solo la palabra...
		for (int i=0; i<total.length; i++){
			
			 String[] split_sep = total[i].split(" ");//separamos por espacio...
			 
			 //comparamos con la fecha recibida...
			 if (split_sep[2].equalsIgnoreCase(date)){
				 
				 words.add(split_sep[1]);//agregamos el elemento en posicion 1 al arreglo...
				 words_sin_repetir.add(split_sep[1]);
			 }
		}
		
		//esto es para eliminar los elementos repetidos...
		// add elements to al, including duplicates 
		HashSet<String> hs = new HashSet<String>(); 
		hs.addAll(words_sin_repetir); 
		words_sin_repetir.clear(); 
		words_sin_repetir.addAll(hs);

		//ahora genero un String [] de las palabras no repetidas...
		String[] solas = new String[words_sin_repetir.size()];
		for (int i=0; i<words_sin_repetir.size(); i++){
			
			solas[i] = words_sin_repetir.get(i);
		}
		
		//ahora cuento cada repeticion...
		for (int i=0; i<solas.length; i++){
		
			int cont=0;
			for (int j=0; j<words.size(); j++){
				
				if (words.get(j).equals(solas[i])){
					cont++;
				}
			}
			solas[i] = ""+cont+" "+solas[i];
		}
		
		Arrays.sort(solas);
		
		//creo arreglo para ordenarlos de mayor a menor...
		String [] solas2 = new String[solas.length];
				
		int cont = solas.length-1;
				
		for (int i=0; i<solas.length; i++){
					
			solas2[i] = solas[cont];
			cont--;
		}
		
		return solas2;

	}
	
	/**
	 * @author David Medina
	 * @description
	 * Genera las estadisticas por fecha con los datos existentes en la base de datos
	 * @return
	 * @param date
	 */
	public String [] EstadisticasFecha(String date){

		String [] total = this.leer();//obtenemos la totalidad de los datos...
		ArrayList<String> words = new ArrayList<String>();//tenemos arreglo parcial...
		
		//ciclo para obtener solo la palabra...
		for (int i=0; i<total.length; i++){
			
			 String[] split_sep = total[i].split(" ");//separamos por espacio...
			 
			 //comparamos con la fecha recibida...
			 if (split_sep[2].equalsIgnoreCase(date)){
				 
				 words.add(split_sep[1]+" "+date);//agregamos el elemento en posicion 1 al arreglo...
			 }
		}
		
		//esto es para eliminar los elementos repetidos...
		// add elements to al, including duplicates 
		HashSet<String> hs = new HashSet<String>(); 
		hs.addAll(words); 
		words.clear(); 
		words.addAll(hs);

		String[] res_query = new String [words.size()];
		
		for (int i=0; i<words.size(); i++){
			
			res_query[i] = words.get(i);
		}
		
		return res_query;
	}
	
	/**
	 * @author David Medina
	 * @description
	 * Hace una consulta de las palabras que existen en la base de datos 
	 * @return
	 */
	public String [] Words(){
	
		String [] total = this.leer();//obtenemos la totalidad de los datos...
		String [] words = new String [total.length];//tenemos arreglo parcial...
		
		//ciclo para obtener solo la palabra...
		for (int i=0; i<total.length; i++){
			
			 String[] split_sep = total[i].split(" ");//separamos por espacio...
			 
			 			 
			 words[i] = split_sep[1];//agregamos el elemento en posicion 1 al arreglo...
		}
		
		return words;
	}
	
	/**
	 * @author David Medina
	 * @description
	 * Metodo que genera las estadisticas de por palabra
	 * @param Word
	 * @return
	 */
	public String [] EstadisticasWord(String Word){
		
		String [] total = this.leer();//obtenemos la totalidad de los datos...
		ArrayList<String> words = new ArrayList<String>();//tenemos arreglo parcial...
		ArrayList<String> words_sin_repetir = new ArrayList<String>();//coleccion parcial...
		
		//ciclo para obtener solo la palabra...
		for (int i=0; i<total.length; i++){
			
			 String[] split_sep = total[i].split(" ");//separamos por espacio...
			 
			 //comparamos con la fecha recibida...
			 if (split_sep[1].equalsIgnoreCase(Word)){
				 
				 words.add(split_sep[1]+"-"+split_sep[2]);//agregamos el elemento en posicion 1 al arreglo...
				 words_sin_repetir.add(split_sep[1]+"-"+split_sep[2]);//agregamos el elemento en posicion 1 al arreglo...
			 }
		}
		
		//esto es para eliminar los elementos repetidos... 
		HashSet<String> hs = new HashSet<String>(); 
		hs.addAll(words_sin_repetir); 
		words_sin_repetir.clear(); 
		words_sin_repetir.addAll(hs);

		//ahora genero un String [] de las palabras no repetidas...
		String[] solas = new String[words_sin_repetir.size()];
		for (int i=0; i<words_sin_repetir.size(); i++){
					
			solas[i] = words_sin_repetir.get(i);
		}
				
		//ahora cuento cada repeticion...
		for (int i=0; i<solas.length; i++){
				
			int cont=0;
			
			for (int j=0; j<words.size(); j++){
						
				if (words.get(j).equals(solas[i])){
					cont++;
				}
			}
			solas[i] = ""+cont+" "+solas[i];
		}	
		return solas;
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que obtiene los datos existentes en la base de datos y los almacena en una coleccion...
	 * @return
	 */
	public ArrayList<String> SendWords(){
		
		ArrayList<String> resultados = new ArrayList<String>();//coleccion contenedora...
		
		String columns[] = {_ID, "contenido", "date"};//obtenemos las columnas...
		
		Cursor c = this.getReadableDatabase().query("palabras", columns, null, null, null, null, null);//hacemos la consulta a la tabla...
		
		c.moveToFirst();//obtenemos los id...
		int id2, id3;
		
		id2 = c.getColumnIndex("contenido");
		id3 = c.getColumnIndex("date");
		
		//ciclo iterativo de captura de datos...
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
			
			String res = c.getString(id2)+" "+c.getString(id3);//formamos el string...
			
			resultados.add(res);//agregamos la variable a la coleccion...
		}
		
		return resultados;//retornamos...
	}
}