package com.android.keyloggeraplication;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;

import com.android.databasemanager.HandlerSqLite;
import com.example.android.softkeyboard.R;

/**
 * @author David Medina
 * @Descripcion
 * Esta clase representa la actividad principal dentro de la aplicacion, en la cual se puede mantener
 * un contro de todos los controladores existentes en ella, ademas.
 */
public class MainActivity extends Activity implements OnClickListener{
	
	//para el manejo de los controladores principales de la activity...
	private View ranking_global;
	private View ranking_fecha;
	private View estadistica_fecha;
	private View estadistica_palabra;
	private View enviar;
	private View eliminar;
	
	//para el manejo de la base de datos...
	private HandlerSqLite handler;
	
	//para hacer el calendario...
	private int year;
	private int month;
	private int day;
	private String Fecha;
	static int verificador=0;
	static final int DATE_PICKER_ID = 1111;
		
	/**
	 * @author david
	 * @param 
	 * @return 
	 * @description
	 * Metodo que inicializa los controladores de la activity...
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//instancia database manager...
		handler = new HandlerSqLite(this);
		
		//asociacion controladores-atributos clases...
		this.ranking_global = findViewById(R.id.rankingblobal);
		this.ranking_fecha = findViewById(R.id.ranking_by_date);
		this.estadistica_fecha = findViewById(R.id.statistics_by_date);
		this.estadistica_palabra = findViewById(R.id.statistics_by_word);
		this.enviar = findViewById(R.id.send_toDB);
		this.eliminar = findViewById(R.id.eliminar);

		//agregar oyentes...
		this.ranking_global.setOnClickListener(this);
		this.ranking_fecha.setOnClickListener(this);
		this.estadistica_fecha.setOnClickListener(this);
		this.estadistica_palabra.setOnClickListener(this);
		this.enviar.setOnClickListener(this);
		this.eliminar.setOnClickListener(this);
		
		//obtencion de datos para el calendario...
        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);
       
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Oyente de la clase, discrimina entre la fuente del evento...
	 */
	public void onClick(View v){
		
		//si se selecciona el ranking global...
		if (v.getId() == ranking_global.getId()){
		
			handler.abrir();//obtencion de datos de la db...
			AlertDialog.Builder builder = new AlertDialog.Builder(this);//alerta...
			builder.setTitle("Ranking Global");
			builder.setItems(handler.RankingGlobal(), new DialogInterface.OnClickListener() {//agregamos los items de respuesta...
			
				@Override
				public void onClick(DialogInterface dialog, int which) {//a cada uno de los itemes se le agrega un oyente null por defecto...
					// TODO Auto-generated method stub
					
				}
			});
		
			Dialog dialog = builder.create();//creamos y mostramos el dialog...
			dialog.show();
		}
		
		//si pulsa el ranking por fecha...
		if (v.getId() == ranking_fecha.getId()){
			
			verificador=1;//variable global...
			//se muestra el calendario... 
            showDialog(DATE_PICKER_ID);
                        
		}

		//si pulsa las estadisticas por palabra...
		if (v.getId() == estadistica_palabra.getId()){
			
			handler.abrir();//obtencion de palabras...
			final String [] palabras = handler.Words();
			//construccion de alerta...
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Estadisticas");
			//se agregan los items...
			builder.setItems(palabras, new DialogInterface.OnClickListener() {
			
				//se agrega el oyente a cada item, para capturar lo que pulso y hacer la query pertinente...
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					String Word=palabras[which];
					
					//nuevo cuadro de dialogo con los resultados de la consulta a la base de datos...
					AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
					builder2.setTitle("Estadisticas");
					builder2.setItems(handler.EstadisticasWord(Word), new DialogInterface.OnClickListener() {
					
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							
						}
					});
				
					//mostramos segundo cuadro de dialogo...
					Dialog dialog2 = builder2.create();
					dialog2.show();
					
				}
			});
		
			//mostramos primer cuadro de dialogo
			Dialog dialog = builder.create();
			dialog.show();

		}

		//si pulsa las estadisticas  por fecha....
		if (v.getId() == estadistica_fecha.getId()){
			
			verificador=2;//opcion de variable global...
			//mostramos el calendario... 
            showDialog(DATE_PICKER_ID);			
			
		}

		//si pulsa la opcion enviar...
		if (v.getId() == enviar.getId()){
			
			Intent loggin = new Intent(this, LogginDB.class);//se genera una nueva activity...
			startActivity(loggin);//inicio...
			
		}

		//si pulsa la opcion eliminar de la base de datos...
		if (v.getId() == eliminar.getId()){
			
			new AlertaConfirmacion(this).Alerta();//se genera una alerta de confirmacion...

		}

	}
	
	//setter and getter de handler...
	public HandlerSqLite getHandler() {
		return handler;
	}

	
	public void setHandler(HandlerSqLite handler) {
		this.handler = handler;
	}

	//se crea un calendario...
	@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_PICKER_ID:
             
            // open datepicker dialog. 
            // set date picker for current date 
            // add pickerListener listner to date picker
            return new DatePickerDialog(this, pickerListener, year, month,day);
        }
        return null;
    }
 
	//oyente para el picklistener...
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
 
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                int selectedMonth, int selectedDay) {
             
            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;
 
            // Show selected date 
            Fecha =new StringBuilder().append(day).append("-").append(month+1).append("-").append(year).toString();

            //para la base de datos 
            handler.abrir();
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle(Fecha);
			String [] query;
			//hacemos la consulta en base a la opcion que se pide...
			if (verificador==1){
				query=handler.RankingFecha(Fecha);
			}else{
				query=handler.EstadisticasFecha(Fecha);
			}
			//agregamos los items
			builder.setItems(query, new DialogInterface.OnClickListener() {
			
				//agregamos los oyentes a los items solo vacios...
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
		
			//mostramos el dialogo...
			Dialog dialog = builder.create();
			dialog.show();            
       
           }
    };
    
    
}
