package com.android.keyloggeraplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.Toast;

/**
 * @author David Medina Ortiz
 * @description
 * Clase que tiene la responsabilidad de generar una alerta de confirmacion con distintos mensajes...
 * @version alpha
 */
public class AlertaConfirmacion {

	MainActivity activity;//para obtener el contexto...
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Constructor de la clase, inicializa el atributo activity
	 * @param activity
	 */
	public AlertaConfirmacion(MainActivity activity){
		
		this.activity = activity;
		
	}
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite la instancia de una alerta de confirmacion...
	 */
	public void Alerta(){
		
		AlertDialog alerta = new AlertDialog.Builder(activity).create();//instancia de alerta...
		alerta.setMessage("Confirma eliminación de datos?");//modificamos titulo...
		
		//botones de accion...
		alerta.setButton("Ok", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				refresh();
				Mensaje("Datos Eliminados", activity);
			}
		});
		
		alerta.setButton2("Cancel", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				
				Mensaje("Operacion Cancelada", activity);
			}
		});
		
		//mostramos...
		alerta.show();		
	}

	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite mostrar un mensaje en version toast...
	 * @param text
	 * @param activity
	 */
	public void Mensaje(String text, MainActivity activity){
		
		Toast toast = Toast.makeText(activity, text, Toast.LENGTH_SHORT);//instanciamos un toast de corta duracion...
		toast.setGravity(Gravity.CENTER_VERTICAL, 0,0);//la centramos en 0,0
		toast.show();//mostramos...
	}
	
	/**
	 * @author David Medina Ortiz
	 * @description
	 * Metodo que permite hacer la eliminacion de datos a la base de datos...
	 * @return
	 */
	public String refresh(){
		
		String resultado=null;//contiene el resultado...
		
		//manejo de exception...
		try {
			activity.getHandler().Eliminar();//obtenemos el handler de la base de datos y eliminamos...
			resultado = "Datos borrados de forma correcta";//mostramos el resultado...
		} catch (Exception e) {
			//capturamos la exception...
			resultado = e.getMessage();
		}
		return resultado;//retornamos el resultados
	}
}